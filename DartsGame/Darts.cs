﻿using System;

namespace DartsGame
{
    public static class Darts
    {
        /// <summary>
        /// Calculates the earned points in a single toss of a Darts game.
        /// </summary>
        /// <param name="x">x-coordinate of dart.</param>
        /// <param name="y">y-coordinate of dart.</param>
        /// <returns>The earned points.</returns>
        public static int GetScore(double x, double y)
        {
            if ((x > -10 && x < -5 && y > -10 && y < -5) || (x > 5 && x < 10 && y < 10 && y > 5))
            {
                return 0;
            }
            else if (((x == -10 || x == 10) && y == 0) || ((y == -10 || y == 10) && x == 0))
            {
                return 1;
            }
            else if ((x > -5 && x < -1 && y > -5 && y < -1) || (x > 1 && x < 5 && y < 5 && y > 1))
            {
                return 1;
            }
            else if (((x == -5 || x == 5) && y == 0) || ((y == -5 || y == 5) && x == 0))
            {
                return 5;
            }
            else if ((x == -0.1 || x == 0.1) && (y == -0.1 || y == 0.1))
            {
                return 10;
            }
            else if (((x == -1 || x == 1) && y == 0) || ((y == -1 || y == 1) && x == 0))
            {
                return 10;
            }
            else if (x > -1 && x < 1 && y > -1 && y < 1)
            {
                return 5;
            }
            else
            {
                return 0;
            }
        }
    }
}
